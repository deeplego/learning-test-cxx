#ifndef USER_H
#define USER_H

#include <string>

#include "Collection.h"

struct cmpCollection {
  bool operator() (const Collection *lhs, const Collection *rhs) const {
   return lhs->getName() < rhs->getName(); 
  }
};

class User {
private:
  std::string m_name;
  std::set<Collection *, cmpCollection> *m_collections;
public:
  User(std::string name): m_name(name) {
    m_collections = new std::set<Collection *, cmpCollection>;
  }
  ~User();
  void addCollection(Collection *collection) { m_collections->insert(collection); }
  std::set<Film *, cmp> *queryDownloadedFilmByGenre(std::string genre);
};

#endif
